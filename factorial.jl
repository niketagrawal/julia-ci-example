"""
    factorial(n::Int)

Compute the factorial of n.

Source: https://coderefinery.github.io/testing/test-design/#pure-and-impure-functions

"""
function factorial(n::Int)
    if n < 0
        throw(DomainError("n must be non-negative"))
    end
    result = 1
    for i in 1:n
        result *= i
    end
    return result
end

using Test


@testset "Test the factorial function" begin
    @test_throws DomainError factorial(-1)
    @test factorial(3) == 6
end

