## Unit test for the addition function in addition.jl

using Test
include("addition.jl")

@testset "Test the addition function" begin
    @test add(1, 2) == 3
    @test add(1, -1) == 0
end
